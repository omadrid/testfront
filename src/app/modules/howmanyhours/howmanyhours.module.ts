import { NgModule } from '@angular/core';

import { HowManyHoursComponent } from './howmanyhours.component';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule as NgCommonModule } from '@angular/common';
import { CommonModule } from '../../common/common.module';

const routes: Routes = [
  {
    path: 'howmanyhours',
    component: HowManyHoursComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    NgCommonModule,
    CommonModule
  ],
  declarations: [
    HowManyHoursComponent
  ],
})

export class HowManyHoursModule {}
