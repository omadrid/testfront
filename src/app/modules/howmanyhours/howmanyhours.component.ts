import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { Client } from '../../services/api';
import { MindsTitle } from '../../services/ux/title';
import { Session } from '../../services/session';

@Component({
  moduleId: module.id,
  selector: 'm-howmanyhours',
  templateUrl: 'howmanyhours.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class HowManyHoursComponent implements OnInit {
  minds = window.Minds;
  username: string = '';
  loading: boolean = false;
  videoError: boolean = false;

  ms: number;

  // initialize time increments with placeholder value to prevent content jumping
  days: string = "00";
  hours: string = "00";
  minutes: string = "00";
  seconds: string = "00";

  interval;

  constructor(
    public session: Session,
    public client: Client,
    public router: Router,
    public title: MindsTitle,
    private cd: ChangeDetectorRef
  ) {
  }

    async load() {
      this.loading = true;

      // get time since current user account creation
      let response:any = await this.client.get('api/v2/howmanyhours');
      this.ms = response.seconds * 1000;

      this.loading = false;

      //update the clock immediately, then every second thereafter
      this.updateClock();
      this.interval = setInterval(() => {
        this.updateClock();
      }, 1000);
    }

  ngOnInit() {
    // set page title
    this.title.setTitle('How many hours');

    // redirect to login page if user isn't logged in
    if (!this.session.isLoggedIn()) {
      return this.router.navigate(['/login']);
    }

    // get username to be displayed in thanks message
    this.username = window.Minds.user.username;

    this.load();
  }

  updateClock() {
    // check for changes between now and current user account create date
    // and calculate time since creation for each time increment

    let now = new Date().getTime();
    let distance = now - this.ms;

    //colculate 'days' value and whether to pad with leading zero
    let days = Math.floor(distance / (1000 * 60 * 60 * 24));
    if (days < 10) {
      this.days = "0" + days;
    } else {
      this.days = "" + days;
    }

    // calculate values for hour/min/sec increments and pad with leading zeros
    this.hours = "0" + Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    this.minutes = "0" + Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    this.seconds = "0" + Math.floor((distance % (1000 * 60)) / 1000);

    // apply the changes on the clock
    this.cd.markForCheck();
    this.cd.detectChanges();
  };

  ngOnDestroy() {
      // stop tracking changes on the clock once user leaves the page
      if (this.interval)
        clearInterval(this.interval);
    }
}
